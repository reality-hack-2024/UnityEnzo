<img src="logo.png" alt="Logo" width="250" height="250"/>


# Unity Enzo


Spatial computing is empowering enterprise use cases more and more. As announced by Meta, Apple Vision Pro, Qualcomm XR1, Magic Leap 2, and more.. Enterprise bundles are creating a new market that aims to augmented productivity in many ways. In this thriving and rising ecosystem, one of the main challenges remains cross-platform productivity across different teams that might have different devices. 
“Unity Enzo” aims to connect through productivity, reimagining a workflow that unifies any device, from a computer to a headset and mobile applications. We wanted to deliver something meaningful that would set the table for future development on top of this kit. 
As unity developers we know the struggle to communicate ongoing work from the editor space to the headset space, we were hoping to bring some impact in this direction as well! 


## Setup

Codeberg/GitHub 

Houdini 
Unity 2022.3.f1
Chat GPT Vision API 
Meshy.ai (Hackathon sponsor) 


### Hardware Required

Magic Leap 2
Meta Quest 3 

### Software Dependencies

Chat GPT Vision API 
Flask 
XR All-in-one MetaQuest2 SDK 
Magic Leap SDK 


## Run

1. Chat GPT setup
   - Change your API Keys and IP local address before to use 
   - Find the server sample file in project 
   - Run server for chat GPT with "python server.py"


2. Test scenes
   - You will need to re-import Photon Pun 2 Free and generate a new AppID 
   - Run scene on a desktop device 
   - Run MetaQuest 3 build on a MetaQuest 3
   - Run MagicLeap 2 build on a MagicLeap 2



## Credits 

Alessio Grancini
https://linktr.ee/alessiograncini

Arthur Baney
https://arthurzbaney.com/

Muhammad Saidur Rahman
https://www.linkedin.com/in/muhammad-saidur-rahman-22150b5a/

Marlon Romero
https://www.linkedin.com/in/mromerocastro/

Ayush Garg 
https://www.linkedin.com/in/lazeash/


## More 

Check out our Devpost 
[link will be here after submission]






