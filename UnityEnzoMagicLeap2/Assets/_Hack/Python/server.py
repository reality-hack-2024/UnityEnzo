from flask import Flask, request, jsonify, send_from_directory
import os
import openai
import base64
import json
import traceback

app = Flask(__name__)

UPLOAD_FOLDER = 'uploads'
os.makedirs(UPLOAD_FOLDER, exist_ok=True)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

openai.api_key = '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'

@app.route('/upload', methods=['POST'])
def file_upload():
    file = request.files['file']
    filename = file.filename
    filepath = os.path.join(UPLOAD_FOLDER, filename)
    file.save(filepath)

    with open(filepath, "rb") as image_file:
        base64_image = base64.b64encode(image_file.read()).decode('utf-8')

    try:
        response = openai.ChatCompletion.create(
            model="gpt-4-vision-preview",
            messages=[
                {
                    "role": "user",
                    "content": [
                        {"type": "text", "text": "What’s in this image?"},
                        {"type": "image_url", "image_url": f"data:image/png;base64,{base64_image}"}
                    ]
                },
            ],
            max_tokens=300
        )
        print("OpenAI API Raw Response:", response)  # Log the raw response

        # description = response.choices[0].message['content']['text']
        description = response.choices[0].message['content']

        json_response = {'description': description}
        print("JSON Response:", json.dumps(json_response))
        return jsonify(json_response)
    except Exception as e:
        traceback.print_exc()
        return jsonify({'error': str(e)})

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)

if __name__ == '__main__':
    app.run(debug=True)
