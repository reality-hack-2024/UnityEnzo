# from openai import OpenAI

# # Directly set your API key here (replace 'your-api-key' with your actual key)
# api_key = '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'

# client = OpenAI(api_key=api_key)

# # Constructing the request payload
# response = client.chat.completions.create(
#     model="gpt-4-vision-preview",
#     messages=[
#         {
#             "role": "user",
#             "content": [
#                 {"type": "text", "text": "What’s in this image?"},
#                 {
#                     "type": "image_url",
#                     "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Gfp-wisconsin-madison-the-nature-boardwalk.jpg/2560px-Gfp-wisconsin-madison-the-nature-boardwalk.jpg"
#                 }
#             ]
#         }
#     ],
#     max_tokens=300
# )

# print(response.choices[0].message.content)

import base64
import requests

# OpenAI API Key
api_key = "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"

# Function to encode the image
def encode_image(image_path):
  with open(image_path, "rb") as image_file:
    return base64.b64encode(image_file.read()).decode('utf-8')

# Path to your image
image_path = "image.png"

# Getting the base64 string
base64_image = encode_image(image_path)

headers = {
  "Content-Type": "application/json",
  "Authorization": f"Bearer {api_key}"
}

payload = {
  "model": "gpt-4-vision-preview",
  "messages": [
    {
      "role": "user",
      "content": [
        {
          "type": "text",
          "text": "What’s in this image?"
        },
        {
          "type": "image_url",
          "image_url": {
            "url": f"data:image/jpeg;base64,{base64_image}"
          }
        }
      ]
    }
  ],
  "max_tokens": 300
}

response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload)

print(response.json())

