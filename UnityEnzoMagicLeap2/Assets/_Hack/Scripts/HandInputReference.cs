using UnityEngine;

public class HandInputReference : MonoBehaviour
{
    public GameObject RightHandIndexTip;
    public GameObject LeftHandIndexTip;
    public GameObject RightHandThumbTip;
    public GameObject LeftHandThumbTip;
    public GameObject RightHandCenter;
    public GameObject LeftHandCenter;
}
