using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class ImageUploader : MonoBehaviour
{
    public UnityEngine.UI.Image uiImage; // Assign your UI Image in the Inspector
    public OpenAIDescription openAIDescription; // Reference to OpenAIDescription script

    //private string serverURL = "http://localhost:5001/upload";

    // mit server
    //private string serverURL = "http://10.29.215.108:5001/upload";
    // hotel server
     private string serverURL = "http://10.29.215.108:5001/upload";
  


    [ContextMenu("Send Image to Server")] // This attribute adds a button to the Inspector
    public void SendImageToServer()
    {
        StartCoroutine(UploadImage());
    }

    IEnumerator UploadImage()
    {
        Texture2D texture = SpriteToTexture2D(uiImage.sprite);
        byte[] imageBytes = texture.EncodeToPNG();

        WWWForm form = new WWWForm();
        form.AddBinaryData("file", imageBytes, "image.png", "image/png");

        using (UnityWebRequest www = UnityWebRequest.Post(serverURL, form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Error: " + www.error);
            }
            else
            {
                string imageUrl = www.downloadHandler.text;
                Debug.Log("Image uploaded successfully. URL: " + imageUrl);

                // Now, request the image description from the OpenAIDescription script
                //openAIDescription.RequestDescription(imageUrl);
                openAIDescription.SetText(imageUrl);
            }
        }
    }

    Texture2D SpriteToTexture2D(Sprite sprite)
    {
        Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
        Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                    (int)sprite.textureRect.y,
                                                    (int)sprite.textureRect.width,
                                                    (int)sprite.textureRect.height);
        newText.SetPixels(newColors);
        newText.Apply();
        return newText;
    }
}
