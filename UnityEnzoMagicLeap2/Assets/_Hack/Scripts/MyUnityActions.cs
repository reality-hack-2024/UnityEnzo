// using UnityEngine;
// using UnityEngine.UI;
// using System.Linq;

// public class MyUnityActions : MonoBehaviour
// {
//     public GameObject targetObject; // Assign this in the inspector

//     [System.Serializable]
//     public class WordAction
//     {
//         public string[] words;
//         public float scaleFactor;
//         public Text feedbackText; // Text component for feedback
//     }

//     public WordAction[] wordActions; // Set this in the inspector

//     public Text uiText;

//     void Start()
//     {
//         uiText = GetComponent<Text>();
//         if (uiText == null)
//         {
//             Debug.LogError("Text component not found on the GameObject.");
//         }
//     }

//     void Update()
//     {
//         if (uiText != null && targetObject != null && wordActions != null)
//         {
//             string textContent = uiText.text.ToLower();
//             foreach (var action in wordActions)
//             {
//                 if (action.words.Any(word => textContent.Contains(word.ToLower())))
//                 {
//                     ScaleObject(action.scaleFactor, action.feedbackText);
//                     break; // Assuming only one action per update. Remove this if multiple actions are needed.
//                 }
//             }
//         }
//     }

//     private void ScaleObject(float scaleFactor, Text feedbackText = null)
//     {
//         GameObject parentObject = new GameObject("ParentObject");
//         parentObject.transform.position = targetObject.transform.position;
//         targetObject.transform.SetParent(parentObject.transform);
//         parentObject.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);

//         if (feedbackText != null)
//         {
//             feedbackText.text = $"Action triggered: Scale factor {scaleFactor}";
//             feedbackText.color = Color.red; // Change the color of the feedback text
//         }
//     }
// }

using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MyUnityActions : MonoBehaviour
{
    public GameObject targetObject; // Assign this in the inspector

    [System.Serializable]
    public class WordAction
    {
        public string[] words;
        public float scaleFactor;
        public Text feedbackText; // Text component for feedback
        public BoolCondition condition = new BoolCondition(); // Track if action has been triggered
    }

    public WordAction[] wordActions; // Set this in the inspector

    public Text uiText;


    void Update()
    {
        if (uiText != null && targetObject != null && wordActions != null)
        {
            string textContent = uiText.text.ToLower();
            foreach (var action in wordActions)
            {
                // Update condition based on word presence
                action.condition.Value = action.words.Any(word => textContent.Contains(word.ToLower()));

                // Check if condition just changed to true
                if (action.condition.ChangedTrue)
                {
                    ScaleObject(action.scaleFactor, action.feedbackText);
                }
            }
        }
    }

    private void ScaleObject(float scaleFactor, Text feedbackText = null)
    {
        // Scale the target object
        targetObject.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);

        if (feedbackText != null)
        {
            feedbackText.text = $"Action triggered: Scale factor {scaleFactor}";
            feedbackText.color = Color.red; // Change the color of the feedback text
        }
    }
}
