using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class OpenAIDescription : MonoBehaviour
{
    public UnityEngine.UI.Text descriptionText;
    public void SetText(string text)
    {
        descriptionText.text = text;
    }
    }

/*
      // Assign your TextMeshProUGUI object here
    private string serverUploadURL = "http://localhost:5000/upload"; // URL to your Flask server upload endpoint


    public void RequestDescription(string imageUrl)
    {
        StartCoroutine(GetImageDescription(imageUrl));
    }

    IEnumerator GetImageDescription(string imageUrl)
    {
        WWWForm form = new WWWForm();
        form.AddField("image_url", imageUrl);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(serverUploadURL, form))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Error: " + webRequest.error);
            }
            else
            {
                string jsonResponse = webRequest.downloadHandler.text;
                Debug.Log("Received response: " + jsonResponse);

                // Try to parse the JSON response
                try
                {
                    // Attempt to parse the JSON response using JsonUtility
                    DescriptionResponse response = JsonUtility.FromJson<DescriptionResponse>(jsonResponse);
                    if (response != null)
                    {
                        descriptionText.text = response.description; // Display the description
                    }
                    else
                    {
                        Debug.LogError("Invalid JSON response format");
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("Error parsing JSON: " + ex.Message);
                }
            }
        }
    }



    [System.Serializable]
    private class DescriptionResponse
    {
        public string description;
    }
*/
