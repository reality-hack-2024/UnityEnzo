using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class OwnershipRequest : MonoBehaviour
{
    public Transform IndexTipR;
    public Transform IndexTipL;
    private PhotonView photonView;
    public float Distance = 0.3f;

    void OnEnable()
    {
        photonView = GetComponent<PhotonView>();
    }

    [ContextMenu("Get Ownership Test")]
    public void GetOwnership()
    {
        if (!photonView.IsMine && canGrab())
        {
            photonView.RequestOwnership();
        }
    }
    private bool canGrab(){
        return Vector3.Distance(IndexTipR.position, transform.position) < Distance
        || Vector3.Distance(IndexTipL.position, transform.position) < Distance;
    }
}