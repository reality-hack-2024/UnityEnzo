
using UnityEngine;
using System.Collections;


public class SimpleCameraCapture : MonoBehaviour
{
    public bool init = false;
    public MagicLeap.Examples.CameraCaptureExample cameraCaptureExample;
    public ImageUploader imageUploader;
    public float repeatTime = 10;
    public void StartCaptureCoroutine()
    {
        StartCoroutine(CaptureEverySecond());
    }

    IEnumerator CaptureEverySecond()
    {
    
        yield return new WaitForSeconds(repeatTime);
        cameraCaptureExample.CaptureImage();
        yield return new WaitForSeconds(2);
        imageUploader.SendImageToServer();
        // turn on the server before to run the experience 
        StartCoroutine(CaptureEverySecond());

    }
}