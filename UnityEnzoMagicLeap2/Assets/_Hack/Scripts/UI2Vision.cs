using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;


public class UI2Vision : MonoBehaviour
{
    public Image uiImage; // Assign your UI Image in the Inspector

    private string apiURL = "https://api.someimagerecognition.com/analyze"; // Hypothetical API endpoint
    private string apiKey = "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"; // Replace with your actual API key

    [ContextMenu("Send Image to API")]
    public void SendImageToAPITest()
    {
        StartCoroutine(SendImageToAPI());
    }

    IEnumerator SendImageToAPI()
    {
        Texture2D texture = SpriteToTexture2D(uiImage.sprite);
        byte[] imageBytes = texture.EncodeToPNG();

        // Create the web request and set headers
        using (UnityWebRequest webRequest = new UnityWebRequest(apiURL, "POST"))
        {
            webRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(imageBytes);
            webRequest.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            webRequest.SetRequestHeader("Content-Type", "application/octet-stream");
            webRequest.SetRequestHeader("Authorization", "Bearer " + apiKey);

            yield return webRequest.SendWebRequest();

            if (webRequest.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Error: " + webRequest.error);
            }
            else
            {
                Debug.Log("Response: " + webRequest.downloadHandler.text);
                // Process the response here
            }
        }
    }

    Texture2D SpriteToTexture2D(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x, 
                                                        (int)sprite.textureRect.y, 
                                                        (int)sprite.textureRect.width, 
                                                        (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
}
